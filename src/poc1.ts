


import { AxiosInstance } from "axios"

class PlugLoader {
    token: string
    iframe: boolean
    domTarget: HTMLBodyElement
    extLoader: ExtensionLoader | null
    handle: any
    constructor(token: string, iframe: boolean) {
        this.token = token
        this.iframe = iframe
    }

    Remove() {
        this.domTarget.childNodes.forEach((node) => {
            this.domTarget.removeChild(node)
        })
    }

    Install() {
        if (this.iframe) {
            return this.installIframe()
        }
        this.installDOM()
    }

    private installIframe() { }

    private installDOM() {
        this.extLoader = new ExtensionLoader(this.domTarget, this.token)
    }
}

interface PlugHandle {
    do(action: string): Promise<any>
    ExtCall(action: string): Promise<any>
}

interface ExtensionCallback {
    (ctx: PlugHandle, rootDom: HTMLElement)
}

class ExtensionLoader {
    token: string
    domTarget: HTMLElement
    totalExts: number
    loadedNumber: number
    initAtOnce: boolean
    callbacks: Map<string, ExtensionCallback>
    pHandle: PlugHandle
    registryId: string
    inited: boolean
    constructor(target: HTMLElement, token: string, initAtOnce?: boolean) {
        this.token = token
        this.domTarget = target
        this.initAtOnce = initAtOnce || false
        this.callbacks = new Map()
        this.registryId = "__registry__"

        //set up registry
        window[this.registryId] = this.registerFn.bind(this)
        this.inited = false
    }

    AllLoaded(): boolean {
        return this.loadedNumber === this.totalExts
    }

    registerFn(pName: string, ext: ExtensionCallback) {
        if (this.inited) {
            console.log("loaded too late => fixme")
            return
        }

        this.loadedNumber = this.loadedNumber + 1
        if (!this.initAtOnce) {
            ext(this.pHandle, this.domTarget)
            if (this.AllLoaded()) {
                this.inited = true
            }
        }

        this.callbacks.set(pName, ext)

        if (this.AllLoaded()) {
            this.initilize()
        }
    }

    async LoadEmbed(): Promise<any> {
        let scripts = await this.embedeScripts();
        this.totalExts = scripts.length
        scripts.forEach((scriptSrc) => {
            const script = new HTMLScriptElement();
            script.src = srcRegistryFn(this.registryId, scriptSrc)
            this.domTarget.appendChild(script)
        })
    }

    private initilize() {
        this.callbacks.forEach((cfn, key) => {
            cfn(this.pHandle, this.domTarget)
        })

        this.inited = true
    }

    private async embedeScripts(): Promise<string[]> {
        return []
    }
}

const srcRegistryFn = (registry: string, src: string): string => {
    return `${registry}?${src}`
}

const PlugService = () => {

}


class Plug {
    plugId: string
    agentId: string
    token: string
    extLoader: ExtensionLoader
    http: AxiosInstance
    domRoot: HTMLElement
    signedTokens: Map<string, any>
    authProxy: PlugAuthProxy


    constructor(token: string, target: HTMLElement) {
        this.token = token
        this.domRoot = target
    }

    loadExtensions(): Promise<any> {
        this.extLoader = new ExtensionLoader(this.domRoot, this.token, true)
        return this.extLoader.LoadEmbed()
    }

    async doSignedQuery(squery: string, params: any): Promise<any> {
        const payload = { type: "signed_query", data: { query: squery, params: params } }
        const sig = this.authProxy.signAction(payload)

        this.http.post("/action", payload, {
            headers: { "X-Signature": sig }
        })
    }



    async doAction(action: string, data?: any): Promise<any> {
        this.authProxy.signAction({ type: "plug_action", action: action, data: data })
    }
}

interface Payload {
    data?: any
    type: string
    action?: string
}

class PlugAuthProxy {
    key: string
    constructor() { }

    signAction(data: Payload): string {
        const payload = JSON.stringify([data.type, data.action, data.data])
        return ""
    }

    signPreSignedQuery(data: Payload): string {
        return ""
    }

}


class PlugBlob {
    token: string
    constructor() {
    }

    async Upload(): Promise<any> {

    }

    async DownLoad(): Promise<any> {

    }

}


