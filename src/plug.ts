interface Signer {
    Sign(action: string, data?: any): [string, string]
}

export class Plug {
    tenant_id: string
    plug_id: string
    agent_id: string
    token: string
    signer: Signer
    action_url: string

    constructor(tenant_id: string, plug_id: string, agent_id: string,  base_url: string, token: string) {
        this.tenant_id = tenant_id
        this.plug_id = plug_id
        this.agent_id = agent_id
        this.token = token
        this.signer = new NoopSigner()
        this.action_url = base_url + '/console/action/' + tenant_id
    }

    async doAction(action: string, data?: any): Promise<any> {
        const [payload, signature] = this.signer.Sign(action, data)
        const response = await fetch(this.action_url, {
            method: 'POST',
            mode: 'cors',
            headers: {
                'X-Signature': signature,
                'Content-Type': 'application/json',
                'Authorization':this.token,
            },
            redirect: 'follow',
            referrerPolicy: 'strict-origin-when-cross-origin',
            body: payload
        });
        return response.json();
    }
}


class NoopSigner {
    constructor() { }
    Sign(action: string, data?: any): [string, string] {
        const payload = JSON.stringify(["SIG_N_0", action, data])
        return ["", payload]
    }
}